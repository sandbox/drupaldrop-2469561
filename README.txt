
-- SUMMARY --
  This is a simple module for embedding website user links in the body field for cross refrencing .instead of adding hard coded drupal user links just add a username with [username] and it automatically creates a link on the front end. I created this module for one of my project so thought of sharing it here.

-- ROAD MAP --

  - For all editor fields in the content type
  - suggestions of username while embedding user link

-- HOW TO USE -- 
  Just enable the config setting at admin/config/content/user_embed and ready to embed links in the content body field.
