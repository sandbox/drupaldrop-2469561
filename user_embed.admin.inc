<?php

/**
 * @file
 * Provides admin settings of the module.
 */

/**
 * Implements hook_form().
 */
function _user_embed_config_form() {
  $form['embed_user_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable user link'),
    '#default_value' => variable_get('user_embed_links', ''),
  );
  return system_settings_form($form);
}
